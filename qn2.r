
"======================================================================"
"Qn2 CODE"
XN=c()
XC=c()

"Sorting numerical and categorical variables"
for(i in 2:length(names(US.Mortgages))){
  if (is.numeric(US.Mortgages[,i])==1){
    XN = c(XN,i)
  }else{
    XC = c(XC,i)
  }
}


YN<-matrix(NA,4,length(XN))

"Caluculating required parametres "
k = 0
for(j in XN){
  k = k+1
  YN[1,k] <- names(US.Mortgages)[j]
  YN[2,k] <- mean(US.Mortgages[,j])
  YN[3,k] <- median(US.Mortgages[,j])
  YN[4,k] <- sd(US.Mortgages[,j])
}
"Gather required data for categorical invaribles(except percentage, which will later be calculated seperately"
YC=c()
for (n in XC){
  a<-table(US.Mortgages[,n])
  b<-data.frame(a)
  YC = c(YC,b)
"for YC the data set which stores desired data,for instance, variable level for def _lag is YC[2*1-1],frequency is YC[2*2]"
}

ix<-sample(49325, 49325*2/3, replace=FALSE)
cctrain <- US.Mortgages[ix,]
cctest <- US.Mortgages[-ix,]


glm.out <- with(cctrain,
                glm(FALSE ~ log(score),
                    family = binomial("logit")) )
summary(glm.out)
