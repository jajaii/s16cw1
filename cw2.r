"============"
"AUC function"
roc <- function(y, s)
{
  yav <- rep(tapply(y, s, mean), table(s))
  rocx <- cumsum(yav)
  rocy <- cumsum(1 - yav)
  area <- sum(yav * (rocy - 0.5 * (1 - yav)))
  x1 <- c(0, rocx)/sum(y)
  y1 <- c(0, rocy)/sum(1 - y)
  auc <- area/(sum(y) * sum(1 - y))
  print(auc)
  plot(x1,y1,"l")
}

"============"


"Data preparation"

"PPM excluded since only have 1 non-blank level, which means imputed value will still be the only level exists for this variable."
US.Mortgages1<-US.Mortgages
glm.out = glm(first.time.homebuyer~def_flag+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)+occupancy.status+channel+loan.purpose+property.state+property.type+orig.loan.term+number.borrowers+orig.year, family=binomial(link="logit"), data=US.Mortgages1)
summary(glm.out)
yp <- predict(glm.out, US.Mortgages1, type="response")
US.Mortgages1$first.time.homebuyer1<-yp

ma = max(yp)
mi = min(yp)

da_ho<-US.Mortgages1[which(US.Mortgages1$first.time.homebuyer==""),]
da_ho1<-US.Mortgages1[which(US.Mortgages1$first.time.homebuyer!=""),]

asd = da_ho[which(da_ho$first.time.homebuyer1>(ma-mi)/2),]

dsa = da_ho[which(da_ho$first.time.homebuyer1<(ma-mi)/2),]
asd$first.time.homebuyer = 'Y'
dsa$first.time.homebuyer = 'N'
ddd = rbind(asd,dsa)
US.Mortgages1=rbind(ddd,da_ho1)

"(b)Variable Selection"

"Train Test Set"
ix<-sample(49325, 49325*2/3, replace=FALSE)
train <- US.Mortgages1[ix,]
test <- US.Mortgages1[-ix,]


"s16cw1 base model"
glm.out <- with(train,
                glm(def_flag==FALSE ~ score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                family = binomial("logit")) 
summary(glm.out)
aic1 = glm.out$aic

"base model+occupancy.status"
glm.out <- with(train,
                glm(def_flag==FALSE ~ occupancy.status+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                family = binomial("logit")) 
summary(glm.out)
aic2 = glm.out$aic

"since aic2<aic1, occupancy.status is added into the model"

"base model+occupancy.status+channel"
glm.out <- with(train,
                glm(def_flag==FALSE ~ channel+occupancy.status+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                family = binomial("logit")) 
summary(glm.out)
aic3 = glm.out$aic
"since aic3<aic2, channel is added to the model"

"base model+occupancy.status+channel+loan.purpose"
glm.out <- with(train,
                glm(def_flag==FALSE ~ loan.purpose+channel+occupancy.status+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                family = binomial("logit")) 
summary(glm.out)
aic4 = glm.out$aic
"since aic4<aic3, loan.purpose is added to the model"

"base model+occupancy.status+channel+loan.purpose+property.state"
glm.out <- with(train,
                glm(def_flag==FALSE ~ property.state+loan.purpose+channel+occupancy.status+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                family = binomial("logit")) 
summary(glm.out)
aic5 = glm.out$aic
"since aic5<aic4, property.state is added to the model"

"base model+occupancy.status+channel+loan.purpose+property.state+property.type"
glm.out <- with(train,
                glm(def_flag==FALSE ~ property.type+property.state+loan.purpose+channel+occupancy.status+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                family = binomial("logit")) 
summary(glm.out)
aic6 = glm.out$aic
"since aic6<aic5, property.type is added to the model"

"base model+occupancy.status+channel+loan.purpose+property.state+property.type+orig.loan.term"
glm.out <- with(train,
                glm(def_flag==FALSE ~ orig.loan.term+property.type+property.state+loan.purpose+channel+occupancy.status+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                family = binomial("logit")) 
summary(glm.out)
aic7 = glm.out$aic
"since aic7>aic6, orig.loan.term is not added to the model"

"base model+occupancy.status+channel+loan.purpose+property.state+property.type+number.borrowers"
glm.out <- with(train,
                glm(def_flag==FALSE ~ number.borrowers+property.type+property.state+loan.purpose+channel+occupancy.status+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                family = binomial("logit")) 
summary(glm.out)
aic8 = glm.out$aic
"since aic8<aic6, number.borrowers is added to the model"

"base model+occupancy.status+channel+loan.purpose+property.state+property.type+number.borrowers"
glm.out <- with(train,
                glm(def_flag==FALSE ~orig.year+number.borrowers+property.type+property.state+loan.purpose+channel+occupancy.status+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                family = binomial("logit")) 
summary(glm.out)
aic9 = glm.out$aic
"since aic9>aic8, orig.year is not added to the model"

"base model+occupancy.status+channel+loan.purpose+property.state+property.type+number.borrowers"
glm.out <- with(train,
                glm(def_flag==FALSE ~ first.time.homebuyer+number.borrowers+property.type+property.state+loan.purpose+channel+occupancy.status+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                family = binomial("logit")) 
summary(glm.out)
aic10 = glm.out$aic
"since aic10<aic8, first.time.homebuyer is added to the model"

"(c)Segmentation"
"1. Segmentation on orig.year"
train1 = train[which(train$orig.year=="08"),]
train2 = train[which(train$orig.year=="09"),]
test1 = test[which(test$orig.year=="08"),]
test2 = test[which(test$orig.year=="09"),]


glm.out1 <- with(train1,
                 glm(def_flag==FALSE ~ first.time.homebuyer+number.borrowers+property.type+property.state+loan.purpose+channel+occupancy.status+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                 family = binomial("logit")) 
summary(glm.out1)

glm.out2 <- with(train2,
                 glm(def_flag==FALSE ~ first.time.homebuyer+number.borrowers+property.type+property.state+loan.purpose+channel+occupancy.status+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                 family = binomial("logit")) 
summary(glm.out2)

yp1 <- predict(glm.out1, test1, type="link")
yp2 <- predict(glm.out2, test2, type="link")
roc(test1$def_flag==FALSE, yp1)
"AUC=0.8631492"
roc(test2$def_flag==FALSE, yp2)
"AUC=0.8749135"
roc(test$def_flag==FALSE,c(yp1,yp2))
"AUC=0.897678"
"since both AUC smaller than previous model without segmentation on orig.year,the this segmented model is not used"

gl12 = rbind()
"2. Segmentation on number.borrower"
train11 = train[which(train$number.borrowers==1),]
train22 = train[which(train$number.borrowers==2),]
test11 = test[which(test$number.borrowers==1),]
test22 = test[which(test$number.borrowers==2),]


glm.out11 <- with(train11,
                 glm(def_flag==FALSE ~ first.time.homebuyer+property.type+property.state+loan.purpose+channel+occupancy.status+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                 family = binomial("logit")) 
summary(glm.out11)

glm.out22 <- with(train22,
                 glm(def_flag==FALSE ~ first.time.homebuyer+property.type+property.state+loan.purpose+channel+occupancy.status+score+DTI+LTV+log10(UPB+80000)+sqrt(OIR-4.125)),
                 family = binomial("logit")) 
summary(glm.out22)

yp11 <- predict(glm.out11, test11, type="link")
yp22 <- predict(glm.out22, test22, type="link")
roc(test11$def_flag==FALSE, yp11)
"AUC=0.8825481"
roc(test22$def_flag==FALSE, yp22)
"AUC=0.8970854"
roc(test$def_flag==FALSE,c(yp11,yp22))
"AUC=0.815605"
"(d)Data Testing"
yp <- predict(glm.out, test, type="link")
roc(test$def_flag==FALSE, yp)